'use strict'

const m = require('mithril');
const Localize = require('localize');
const barlist = require('./barlist');
const chartlist = require('./chartlist');

const t = new Localize('./translations');
t.throwOnMissingTranslation(false);
t.setLocale(process.env.LANG);

const controller = (har) => {
    return {
        chartlistCtrl: chartlist.controller(har),
        barlistCtrl: barlist.controller(har.log)
    }
}

const view = (ctrl) => {
    return [
        chartlist.view(ctrl.chartlistCtrl),
        barlist.view(ctrl.barlistCtrl)
    ]
}

module.exports = {
    controller: controller,
    view: view
}
