'use strict'

const m           = require('mithril');
const Localize    = require('localize');

const t = new Localize('./translations');
t.throwOnMissingTranslation(false);
t.setLocale(process.env.LANG);


const controller = (page, entry) => {
  return ({
    width: 90,
    height: 20,
    totalTime: page.pageTimings.onLoad,
    contentTime: page.pageTimings.onContentLoad,
    start: Date.parse(entry.startedDateTime) - Date.parse(page.startedDateTime),
    value: entry.time,
    parts: [
      {
        // DNS
        value: entry.timings.dns,
        color: '#02B3E7'
      },
      {
        // CONNECT
        value: entry.timings.connect,
        color: "#776068"
      },
      {
        // SEND
        value: entry.timings.send,
        color: "#736D79"
      },
      {
        // WAIT
        value: entry.timings.wait,
        color: "#FFEC62"
      },
      {
        // RECEIVE
        value: entry.timings.receive,
        color: "#EB0D42"
      }
    ],
  })
}


const view = (ctrl) => {
  var start = ctrl.start;
  ctrl.totalTime 
  return m('svg', {
      width:          ctrl.width + 10 + '%',
      height:         ctrl.height,
      //viewBox:        '0 0 ' + ctrl.width + ' ' + ctrl.height,
      xmlns:          'http://www.w3.org/2000/svg',
      'xmlns:xlink':  'http://www.w3.org/1999/xlink'
    }, [
      m('rect', {
        x:         0,
        y:         0,
        width:     ctrl.width + 10 + '%',
        height:    ctrl.height,
        'stroke-width': 1,
        stroke: '#000',
        opacity:  0.3,
        fill:      'none'
      }),

      //totaltime -> 100%
      //value -> %
      ctrl.parts.map((map) => {
        const keepS = start;
        start += map.value;
        return m('rect', {
          x:         (ctrl.width * keepS) / ctrl.totalTime + '%',
          y:         0,
          width:     (ctrl.width * map.value) / ctrl.totalTime + '%',
          height:    ctrl.height,
          fill:      map.color
        });
      }),
      m('text', {
        x:      (ctrl.width * start) / ctrl.totalTime + '%',
        y:      ctrl.height * 3 / 4
      }, ctrl.value + ' ms'),
      m('line', {
        x1:       ctrl.width + '%',
        y1:       0,
        x2:       ctrl.width + '%',
        y1:       20,
        'stroke-width': 0.3,
        stroke:   'red'
      }),
      m('line', {
        x1:       (ctrl.width * ctrl.contentTime) / ctrl.totalTime + '%',
        y1:       0,
        x2:       (ctrl.width  * ctrl.contentTime) / ctrl.totalTime + '%',
        y1:       20,
        'stroke-width': 0.3,
        stroke:   'blue'
      })
    ]
  );
}


module.exports = {
    controller: controller,
    view: view
}
