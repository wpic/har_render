'use strict'

const m 				= require('mithril');
const Localize 	= require('localize');

const bar 			= require('./bar');

const t = new Localize('./translations');
t.throwOnMissingTranslation(false);
t.setLocale(process.env.LANG);



const controller = (data) => {
	return data.pages.map(page => {
		return bar.controller(page, data.entries);
	});
}

const view = (ctrl) => {
  return m('ul.accordion', [
  	ctrl.map(barCtrl => {
      return bar.view(barCtrl);
  	})
	]);
}

module.exports = {
  controller: controller,
  view: view
}
