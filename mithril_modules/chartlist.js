'use strict'

const m 				= require('mithril');
const Localize 	= require('localize');

const chart = require('./chart');

const t = new Localize('./translations');
t.throwOnMissingTranslation(false);
t.setLocale(process.env.LANG);



const controller = (data) => {

// TODO: TO CLEAN AND IMPROVE

// Compute data to display the action type piechart.
var blocked = 0, dns = 0, connect = 0, send = 0, wait = 0, receive = 0;
for (var i = 0, len = data.log.entries.length; len > i; ++i) {
	blocked +=  data.log.entries[i].timings.blocked;
	dns += data.log.entries[i].timings.dns;
	connect += data.log.entries[i].timings.connect;
	send += data.log.entries[i].timings.send;
	wait += data.log.entries[i].timings.wait;
	receive += data.log.entries[i].timings.receive;
}

// Compute data to display the languages and technologies used.
var html = 0, javascript = 0, css = 0, image = 0, flash = 0, others = 0;
for (var i = 0, len = data.log.entries.length; len > i; ++i) {
	if (data.log.entries[i].response.content.mimeType.search('html') != -1) {
		html += data.log.entries[i].response.bodySize;
	}
	else if (data.log.entries[i].response.content.mimeType.search('js') != -1) {
		javascript += data.log.entries[i].response.bodySize;
	}
	else if (data.log.entries[i].response.content.mimeType.search('css') != -1) {
		css += data.log.entries[i].response.bodySize;
	}
	else if (data.log.entries[i].response.content.mimeType.search('image') != -1) {
		image += data.log.entries[i].response.bodySize;
	}
	else if (data.log.entries[i].response.content.mimeType.search('flash') != -1) {
		flash += data.log.entries[i].response.bodySize;
	}
	else {
		others += data.log.entries[i].response.bodySize;
	}
}

// Compute data to display the headers and bodies.
var headSent = 0, bodySent = 0, headReceived = 0, bodyReceived = 0;
for (var i = 0, len = data.log.entries.length; len > i; ++i) {
	headSent +=  data.log.entries[i].request.headersSize;
	bodySent += data.log.entries[i].request.bodySize;
	headReceived += data.log.entries[i].response.headersSize;
	bodyReceived += data.log.entries[i].response.bodySize;
}

// Compute data to display the headers and bodies.
var downloaded = 0, partial = 0, cache = 0;
for (var i = 0, len = data.log.entries.length; len > i; ++i) {
	//partial += data.log.entries[i].response.bodySize;
	if (data.log.entries[i].cache !== undefined)
		cache += data.log.entries[i].response.bodySize;
	else
		downloaded +=  data.log.entries[i].response.bodySize;
}

headSent = headSent > 0 ? headSent : 0;
bodySent = bodySent > 0 ? bodySent : 0;
headReceived = headReceived > 0 ? headReceived : 0;
bodyReceived = bodyReceived > 0 ? bodyReceived : 0;

blocked = blocked > 0 ? blocked : 0;
dns = dns > 0 ? dns : 0;
connect = connect > 0 ? connect : 0;
send = send > 0 ? send : 0;
wait = wait > 0 ? wait : 0;
receive = receive > 0 ? receive : 0;

return [
	chart.controller({
		width: 	100,
		height: 100,
		parts:
		[
		  { title: "Blocked",     value : blocked,		color: "#02B3E7" },
		  { title: "DNS",         value:  dns,   			color: "#CFD3D6" },
		  { title: "SSL/TLS",     value : 0,   				color: "#736D79" },
		  { title: "Connect",     value:  connect,   	color: "#776068" },
		  { title: "Send",        value : send,   		color: "#EB0D42" },
		  { title: "Wait",        value : wait,  		 	color: "#FFEC62" },
		  { title: "Receive",     value : receive,    color: "#04374E" }
	    ]
	}),
	chart.controller({
		width: 	100,
		height: 100,
		parts:
		[
		  { title: "HTML/Text",     	value : html / 100,					color: "#02B3E7" },
		  { title: "JavaScript",     	value:  javascript / 100,   color: "#CFD3D6" },
		  { title: "CSS",     				value : css / 100,   				color: "#736D79" },
		  { title: "Image",     			value:  image / 100,   			color: "#776068" },
		  { title: "Flash",        		value : flash / 100,   			color: "#EB0D42" },
		  { title: "Others",        	value : others / 100,  		 	color: "#FFEC62" }
	    ]
	}),
	chart.controller({
		width: 	100,
		height: 100,
		parts:
		[
		  { title: "Headers Sent",     		value : headSent,				color: "#02B3E7" },
		  { title: "Bodies Sent",         value:  bodySent,   		color: "#776068" },
		  { title: "Headers Received",    value : headReceived,  	color: "#FFEC62" },
		  { title: "Bodies Received",     value:  bodyReceived,   color: "#EB0D42" }
	  ]
	}),
	chart.controller({
		width: 	100,
		height: 100,
		parts:
		[
		  { title: "Downloaded",     	value : downloaded,			color: "#02B3E7" },
		  { title: "Partial",         value:  0,   						color: "#776068" },
		  { title: "From Cache",    	value : cache,  				color: "#FFEC62" }
	  ]
	})
];
}

const view = (ctrl) => {

  return m('div.row', [
	  ctrl.map(chartCtrl => {
	    return m('div.col-md-3', [
	    	chart.view(chartCtrl)
	    ]);
	  })
  ]);
}

module.exports = {
  controller: controller,
  view: view
}
