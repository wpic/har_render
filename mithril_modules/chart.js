'use strict'

const m           = require('mithril');
const Localize    = require('localize');

const t = new Localize('./translations');
t.throwOnMissingTranslation(false);
t.setLocale(process.env.LANG);


const controller = (data) => {
  var totalParts = 0;
  // Compute the sum of every parts of the pie.
  for (var i = 0, len = data.parts.length; i < len; ++i)
    totalParts += data.parts[i].value;

  return ({
    width:            data.width,
    height:           data.height,
    baseColor:        data.baseColor ? data.baseColor : '#fff',
    edgeWidth:        data.edgeWidth ? data.edgeWidth : 15,
    delimiterColor:   data.delimiterColor ? data.delimiterColor : '#fff',
    delimiterOpacity: data.delimiterOpacity ? data.delimiterOpacity : '0.3',
    delimiterWidth:   data.delimiterWidth ? data.delimiterWidth : 1,
    parts:            data.parts,
    totalParts:       totalParts
  });
}

const view = (ctrl) => {
  var startRadius =       -Math.PI / 2;
  const centerX =         ctrl.width / 2;
  const centerY =         ctrl.height / 2;
  const pieRadius =       Math.min.apply(null, [centerX, centerY]) - ctrl.edgeWidth;

  return m('div.piechart', [
    m('svg', {
      width:          ctrl.width,
      height:         ctrl.height,
      viewBox:        '0 0 ' + ctrl.width + ' ' + ctrl.height,
      xmlns:          'http://www.w3.org/2000/svg',
      'xmlns:xlink':  'http://www.w3.org/1999/xlink'
    }, [
      m('circle', {
        cx:         centerX,
        cy:         centerY,
        r:          pieRadius - ctrl.edgeWidth,
        fill:       ctrl.baseColor
      }),
      m('g', { opacity: 0.9 }, [
        ctrl.parts.map((part, index) => {
          // Compute paths to draw the piechart part and its edge.
          const segmentAngle = (part.value / ctrl.totalParts) * (Math.PI * 2);
          const endRadius = startRadius + segmentAngle;
          const largeArc = ((endRadius - startRadius) % (Math.PI * 2)) > Math.PI ? 1 : 0;
          const startX = centerX + Math.cos(startRadius) * pieRadius;
          const startY = centerY + Math.sin(startRadius) * pieRadius;
          const endX = centerX + Math.cos(endRadius) * pieRadius;
          const endY = centerY + Math.sin(endRadius) * pieRadius;

          const cmd = [
            'M', startX, startY,
            'A', pieRadius, pieRadius, 0, largeArc, 1, endX, endY,
            'L', centerX, centerY,
            'Z'
          ];

          const startX2 = centerX + Math.cos(startRadius) * (pieRadius + ctrl.edgeWidth);
          const startY2 = centerY + Math.sin(startRadius) * (pieRadius + ctrl.edgeWidth);
          const endX2 = centerX + Math.cos(endRadius) * (pieRadius + ctrl.edgeWidth);
          const endY2 = centerY + Math.sin(endRadius) * (pieRadius + ctrl.edgeWidth);
          
          const cmd2 = [
            'M', startX2, startY2,
            'A', pieRadius + ctrl.edgeWidth, pieRadius + ctrl.edgeWidth, 0, largeArc, 1, endX2, endY2,
            'L', centerX, centerY,
            'Z'
          ];

          startRadius += segmentAngle;

          return m('g', { 'data-order': index }, [
            m('path', {
              d: cmd.join(' '),
              'stroke-width': ctrl.delimiterWidth,
              stroke: ctrl.delimiterColor,
              'stroke-miterlimit': 2,
              fill: part.color
            }),
            m('path', {
              d: cmd2.join(' '),
              'stroke-width': ctrl.delimiterWidth,
              stroke: ctrl.delimiterColor,
              'stroke-miterlimit': 2,
              fill: part.color,
              opacity: ctrl.delimiterOpacity  
            })
          ]);
        })
      ])
    ]),
    m('ul.piechart-legend', [
      ctrl.parts.map((part, index) => {
        return m('li.list-unstyled', [
          m('span', { style: 'background-color:' + part.color }),
          part.title
        ])
      })
    ])
  ]);
}

module.exports = {
    controller: controller,
    view: view
}
