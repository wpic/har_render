'use strict'

const m = require('mithril');
const Localize = require('localize');

const test = require('./test');


const t = new Localize('./translations');
t.throwOnMissingTranslation(false);
t.setLocale(process.env.LANG);

const controller = (page, entries) => {
		return {
			page: 		page,
			entries: 	entries.filter((entry) => {
									if (entry.pageref == page.id)
										return true;
									return false;
								})
		};
}

/*

IF : request.headers && response.headers
--> Display headers tab
IF : request.queryString
--> Display params tab
IF : response.content
--> Display response tab
IF : ???
--> Display HTML tab
IF : response.cache
--> Display cache tab

*/

// Tabs content

const headersTabContent = (data) => {
	return m('div#headersTab', [
		m('div.row', m('div.col-md-12', 'Response Headers')),
		data.response.headers.map((header) => {
			return m('div.row', [
				m('div.col-md-2', header.name),
				m('div.col-md-10', header.value)
			])
		}),
		m('div.row', m('div.col-md-12', 'Requests Headers')),
		data.request.headers.map((header) => {
			return m('div.row', [
				m('div.col-md-2', header.name),
				m('div.col-md-10', header.value)
			])
		})
	]);
}

const paramsTabContent = (data) => {
	return m('div#paramsTab', [
		data.request.queryString.map((query) => {
			return m('div.row', [
				m('div.col-md-2', query.name),
				m('div.col-md-10', query.value)
			])
		})
	]);
}

const responseTabContent = (data) => {
	return m('div#responseTab', m('pre', data.response.content.text));
}

const htmlTabContent = (data) => {
	return m('div#htmlTab', m('iframe', { src: data.request.url }));
}

const cacheTabContent = (data) => {
	return m('div#cacheTab', [
		data.response.cache.afterRequest.forEach((value, key) => {
			return [ 	m('div.col-md-2', key),
								m('div.col-md-10', value)
			];
		})
	]);
}



// TODO: Find a better solution about that.

const headersTab = () => {
	return [
		m('input', { id: 'headersTab', type: 'radio', name:'tabs', checked: 'checked' }),
		m('label', { for: 'headersTab'}, 'Headers')
	];
}

const paramsTab = () => {
	return [
		m('input', { id: 'paramsTab', type: 'radio', name:'tabs', checked: '' }),
		m('label', { for: 'paramsTab'}, 'Params')
	];
}

const responseTab = () => {
	return [
		m('input', { id: 'responseTab', type: 'radio', name:'tabs', checked: '' }),
		m('label', { for: 'responseTab'}, 'Response')
	];
}

const htmlTab = () => {
	return [
		m('input', { id: 'htmlTab', type: 'radio', name:'tabs', checked: '' }),
		m('label', { for: 'htmlTab'}, 'HTML')
	];
}

const cacheTab = () => {
	return [
		m('input', { id: 'cacheTab', type: 'radio', name:'tabs', checked: '' }),
		m('label', { for: 'cacheTab'}, 'Cache')
	];
}

// END TODO

const bar = (page, entry) => {
	const ctrl = test.controller(page, entry);
	return test.view(ctrl);
}

// Display all entries for a page.
const view = (ctrl) => {
	return m('li', { id: ctrl.page.id }, [
		m('input', { type: 'checkbox', checked: '' }),
		m('h4', ctrl.page.title),
		m('ul.accordion', [
			ctrl.entries.map((entry, index) => {
				return m('li', { id: index }, [
					m('input', { type: 'checkbox', checked: '' }),
					m('div.row', [
						m('div.col-lg-3.col-md-3.url', entry.request.method + ' ' + entry.request.url),
						m('div.col-lg-1.col-md-1', entry.response.status + ' ' + entry.response.statusText),
						m('div.col-lg-1.col-md-1', (entry.response.bodySize / 1000).toPrecision(2) + ' KB'),
						m('div.col-lg-7.col-md-7', bar(ctrl.page, entry))
					]),
					m('div.row.test', [
						entry.request.headers !== undefined || entry.response.headers !== undefined ? headersTab() : '',
						entry.request.queryString !== undefined ? paramsTab() : '',
						entry.response.content !== undefined ? responseTab() : '',
						//entry.request.url !== undefined ? htmlTab() : '',
						entry.response.cache !== undefined ? cacheTab() : '',
						m('div.content', [
							entry.request.headers !== undefined || entry.response.headers !== undefined ? headersTabContent(entry) : '',
							entry.request.queryString !== undefined ? paramsTabContent(entry) : '',
							entry.response.content !== undefined ? responseTabContent(entry) : '',
							//entry.request.url !== undefined ? htmlTabContent(entry) : '',
							entry.response.cache !== undefined ? cacheTabContent(entry) : ''
						])
					])





				]);
			}),
			m('li', ctrl.entries. length + ' Requests')
		])
	]);
}
/*
// One way to build tabs...
const view = (ctrl) => {
	return m('div', [
		m('span', ctrl.page.title),
		m('ul', [
			ctrl.entries.map((entry, index) => {
				return m('li', m('div.tabs', [
					m('div.tab', [
						m('input', { type: 'radio', name: 'tab-group-1', id: 'tab-1', checked: 'checked' }),
						m('label', { for: 'tab-1' }, 'Headers'),
						m('div.content', [
							m('table', [
								m('tbody', [
									m('tr', m('td', 'Response Headers')),
									entry.response.headers.map((header) => {
										return m('tr', [
											m('td', header.name),
											m('td', header.value)
										])
									}),
									m('tr', m('td', 'Request Headers')),
									entry.request.headers.map((header) => {
										return m('tr', [
											m('td', header.name),
											m('td', header.value)
										])
									})
								])
							])
						])
					]),
					m('div.tab', [
						m('input', { type: 'radio', name: 'tab-group-1', id: 'tab-2' }),
						m('label', { for: 'tab-2' }, 'Cache'),
						m('div.content', [

						])
					]),
					m('div.tab', [
						m('input', { type: 'radio', name: 'tab-group-1', id: 'tab-3' }),
						m('label', { for: 'tab-3' }, 'Params'),
						m('div.content', [

						])
					]),
					m('div.tab', [
						m('input', { type: 'radio', name: 'tab-group-1', id: 'tab-4' }),
						m('label', { for: 'tab-4' }, 'Response'),
						m('div.content', [

						])
					]),
						m('div.tab', [
						m('input', { type: 'radio', name: 'tab-group-1', id: 'tab-5' }),
						m('label', { for: 'tab-5' }, 'HTML'),
						m('div.content', [

						])
					])
				]));
			})
		])
	]);
}
*/
module.exports = {
    controller: controller,
    view: view
}
